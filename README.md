README
======
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Croatian, edition 1.3. See the wiki pages of the [PARSEME corpora initiative](https://gitlab.com/parseme/corpora/-/wikis/home) for the full documentation of the annotation principles.

The present Croatian data result from an enhancement of the Croatian part of the [PARSEME corpus v 1.1](http://hdl.handle.net/11372/LRT-2842).
For the changes with respect to version 1.1, see the change log below.


Corpora
-------
All annotated data comes from the following source:
* The [hr500k](http://hdl.handle.net/11356/1183) training dataset for Croatian: 3838 sentences from the Croatian part of the SETimes newspaper corpus, other selected newspapers, and a selection of sentences crawled from the web.


Format
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. Here is detailed information about some columns:

* LEMMA (column 3): Available. Manually annotated (Universal Dependencies project).
* UPOS and XPOS (columns 4 and 5): Available. Manually annotated (Universal Dependencies project). The tagset is the one of [UD POS-tags](http://universaldependencies.org/u/pos) and of the MULTEX-EAST v6 specifications.
* FEATS (column 6): Available. Manually annotated (Universal Dependencies project). The tagset is [UD features](http://universaldependencies.org/u/feat/index.html).
* HEAD and DEPREL (columns 7 and 8): Available. Manually annotated (Universal Dependencies project). The tagset is [UD dependency relations](http://universaldependencies.org/u/dep).
* MISC (column 10): No-space information available. Manually annotated (Universal Dependencies project).
* PARSEME:MWE (column 11): Manually annotated. The following [VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.3/?page=030_Categories_of_VMWEs) are annotated: VID, LVC.full, LVC.cause, IRV, IAV.

The data is columns 1-10 are synchronized with the [Croatian UD treebank](https://github.com/UniversalDependencies/UD_Croatian-SET/blob/master/README.md) v2.4. VMWEs in this language have been annotated by a single annotator per file, with the exception of the 300 sentences <!--in the *test* set, and the last 535 sentences of the *train* set,--> which were annotated by three annotators for the sake of inter-annotator agreement estimation.

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 

Tokenization
------------
* Hyphens: Hyphenated compounds preserved as a single token.


Licence
-------
The full dataset is licensed under **Creative Commons Non-Commercial Share-Alike 4.0** licence [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)


Authors
-------
Goranka Blagus, Maja Buljan, Taja Kuzman, Nikola Ljubešić, Ivana Matas, Jan Šnajder


Contact
-------
* nikola.ljubesic@ijs.si

Change log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - Corrections in the text, segmentation and linguistic annotation (columns FORM to MISC) from version 1.1 were transferred from the [hr500k](http://hdl.handle.net/11356/1183) dataset, are therefore synchronised with the Universal Dependencies data (using the same dataset as source), 90% of the sentences were updated with this change (errors corrected, UD formalism updated)
- **2018-04-30**:
  - [Version 1.1](http://hdl.handle.net/11372/LRT-2842) of the corpus was released on LINDAT.

